using System;
using System.Net.Http;
var builder = WebApplication.CreateBuilder();
var app = builder.Build();

app.MapGet("/pong", async context =>
{
    Console.WriteLine("Ping server received Pong");

    await Task.Delay(500); 

    using (var client = new HttpClient())
    {
        await client.GetStringAsync("https://localhost:7106/pong");
    }

    Console.WriteLine("Ping server sent Ping");
    await context.Response.WriteAsync("Ping");
});

await app.RunAsync();
    

